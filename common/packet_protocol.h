#ifndef __PACKET_PROTOCOL_H__
#define __PACKET_PROTOCOL_H__

// increment this every time backwards-incompatible changes
// are made to the packet protocol
#define PACKET_PROTOCOL_REVISION 0

/* packet protocol definitions */

#include <device_params.h>
#include <stddef.h>
#include <stdint.h>

/* the maximum length of a packet from STM32 to host, in bytes.
 when you pass a buffer into the `buf` arg in packet_encode_response,
 it should be at least this long. */
#define PACKET_REQUEST_MAX_LEN 64

/* the maximum length of a packet from host to STM32, in bytes.
 when you pass a bufer into the `buf` arg in packet_encode_request,
 it should be at least this long */
#define PACKET_RESPONSE_MAX_LEN 64


typedef enum PacketKind PacketKind;
typedef struct RequestPacket RequestPacket;
typedef struct ResponsePacket ResponsePacket;


enum PacketKind {
  PK_ECHO = 0,
  PK_WRITE_PARAMETER = 1,
  PK_READ_PARAMETER = 2,
  PK_PRINT = 3
  // add new packet kinds here
};

struct RequestEcho {
  char message[5];  // 4-8: should always be "echo" followed by null terminator
};

struct RequestWriteParameter {
  uint16_t parameter; // 4-5
  uint8_t param_kind; // 6: value from ParamKind enum
  union {
    int32_t int_val;  // 7-10
    uint8_t bool_val; // 7
    uint8_t bytes_val[16]; // 7-22
  };
};

struct RequestReadParameter {
  uint16_t parameter; // 4-5
  uint8_t param_kind; // 6
};



struct ResponseEcho {
  char message[5];  // 4-8: should always be "echo" followed by null terminator
};

struct ResponseWriteParameter {
  uint16_t parameter; // 4-5
  uint8_t param_kind; // 6
};

struct ResponseReadParameter {
  uint16_t parameter; // 4-5
  uint8_t param_kind; // 6: may be PKIND_ERR if an error occurred
  union {
    int32_t int_val;  // 7-10
    uint8_t bool_val; // 7
    uint8_t bytes_val[16]; // 7-22
  };
};

struct ResponsePrint {
  char *str;
};



// a packet sent from the device to us
struct RequestPacket {
  uint8_t kind;         // 0
  uint8_t uid;          // 1
  uint8_t revision;     // 2
  uint8_t contents_len; // 3
  union {
    struct RequestEcho echo;
    struct RequestWriteParameter write_parameter;
    struct RequestReadParameter read_parameter;
  } contents;
};

struct ResponsePacket {
  uint8_t kind;         // 0
  uint8_t uid;          // 1
  uint8_t revision;     // 2
  uint8_t contents_len; // 3
  union {
    struct ResponseEcho echo;
    struct ResponseWriteParameter write_parameter;
    struct ResponseReadParameter read_parameter;
    struct ResponsePrint print;
  } contents;
};


// decode a request packet, writing into request. if successful, returns
// a positive value representing the number of bytes decoded. if not enough
// bytes are available to process the packet, returns 0.
// if an error occurs, the kind, uid, revision, and contents_len fields will
// still be filled. if -1 is returned, the packet kind is not recognized.
// if -2 is returned, the contents_len was invalid for the specified packet kind.
// if -3 is returned, the param read or param write packet did not specify
// a valid param_kind.
// the firmware will call this function when it receives a packet over USB.
int packet_decode_request(uint8_t const *in, int len, RequestPacket *request);

// encode a response packet, writing into buf. returns positive integer
// corresponding to output length on success. returns zero or negative on failure.
// this function will calculate contents_len and revision on its own.
// the firmware will call this function when it wants to send a packet over USB.
int packet_encode_response(uint8_t *buf, ResponsePacket const *const response);


// encode a request packet, writing the data in request into a buffer that
// will be pointed to by buf. returns positive integer corresponding to output
// length on success. returns zero or negative on error.
// the Android device will call this function when it wants to send a packet.
int packet_encode_request(uint8_t *buf, RequestPacket const *const request);

// decode a response packet, writing into response. returns 0 on success.
// the Android device will call this function when it receives a packet over USB.
int packet_decode_response(uint8_t *in, int inlen, ResponsePacket *response);

#endif
