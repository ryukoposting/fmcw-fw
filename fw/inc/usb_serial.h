#ifndef __FW_USBSERIAL_H__
#define __FW_USBSERIAL_H__

#include "tm_stm32_usb.h"
#include "tm_stm32_usb_device_cdc.h"

typedef enum UsbStatus UsbStatus;

enum UsbStatus {
  US_NOT_READY,
  US_FULL_SPEED,
  US_HIGH_SPEED
};

/* prepare the hw peripherals for the USB port.
   returns 0 on success, nonzero on failure. */
int usbserial_init();

/* call this on a regular time interval. don't call
   it before calling usbserial_init */
void usbserial_periodic_task();

/* get the status of the USB connection. will return
   one of the values defined in the UsbStatus enum. */
UsbStatus usbserial_get_status();

/* write to the port. returns number of bytes written,
   or negative on failure. buflen must be less than 65,536. */
int usbserial_write(uint8_t *buf, int buflen);

/* read from the port. returns number of bytes read,
   or negative on failure. buflen must be less than 65,536. */
int usbserial_read(uint8_t *buf, int buflen);

#endif /* __FW_USBSERIAL_H__ */
