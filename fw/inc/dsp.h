#ifndef __FW_dsp_H__
#define __FW_dsp_H__

#include "arm_math.h"
#include "arm_const_structs.h"
#include "stm32fxxx_hal.h"
#include "tm_stm32_fft.h" // probably do not need this. This is the prebuilt library
#include "defines.h"
#include <stdlib.h>

typedef struct
{
  float32_t* frequency;
  float32_t* magnitude;
  float32_t* calculatedDistance;
  uint16_t count;

} dspReturn;

/*
This is the structure that we will use to keep track of our data as it
is transformed and output. This format is the same as what is used in
tm_stm32_fft.c.
*/
typedef struct
{
  const arm_cfft_instance_f32* s; // this struct has the length of the fft, twiddle val, reversal bit, and revesal table length
  float32_t* adc_outputIQ;     // this is the in phase and quadrature adc data. The FFT will be taken using computeFFT
  float32_t* fft_output;     // this is the FFT output that we will use to find range
  dspReturn* dsp_return;     // this is the final dsp output that will include processed data
  uint16_t fft_size;         // size of the fft we are using
  uint16_t adc_output_count;  // number used to count the number of adc values
  uint16_t dsp_out_count; // this is the writeable number used to show final number of dsp values
}  IQDSP_object;

int initialize_DSP(IQDSP_object* dsp_obj, dspReturn* dsp_ret_obj);

void computeFFT(IQDSP_object* dsp_obj);

void parseFFTArray(IQDSP_object* dsp_obj);

void transferBuffer_adcToDSP(IQDSP_object* dsp_obj, uint16_t* adcI, uint16_t* adcQ);

#endif
