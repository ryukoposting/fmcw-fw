#include <fw.h>
#include <usb_serial.h>
#include <mmic.h>
#include <packet_protocol.h>
#include <stdarg.h>

// below here is the extra include files for Michael's FFT testing
#include <stm32fxxx_hal.h>
#include <defines.h>
#include <tm_stm32_fft.h>
#include <arm_math.h>
#include <tm_stm32_delay.h>

#include <dsp.h>

/* FFT settings */
// TODO: move defines into header file and combine FFT_SIZE with N_ADC_SAMPLES
#define FFT_SIZE				(64) /* FFT size is always the same size as we have samples, so 64 in our case */
#define SAMPLES					(FFT_SIZE*2)         /* 64 real parts and 64 imaginary parts */


/* Global variables */
float32_t Input[SAMPLES];   /*!< Input buffer is always 2 * FFT_SIZE */
float32_t Output[FFT_SIZE]; /*!< Output buffer is always FFT_SIZE */

/*!< FFT structure */
TM_FFT_F32_t FFT;

//DSP Structure
IQDSP_object dsp_obj;
dspReturn dsp_ret_obj;

/* Temporary sinus value */
float32_t sin_val;

// END Michael FFT Test Setup files



// PERIPHERALS IN USE:



int __errno;


static int tick_received = 0;

// this runs every 1ms from a timed interrupt.
void TM_DELAY_1msHandler(void)
{
  tick_received += 1;
}


void handle_read_request(RequestPacket *request, ResponsePacket *response)
{
  response->kind = PK_READ_PARAMETER;
  response->uid = request->uid;
  response->revision = PACKET_PROTOCOL_REVISION;
  response->contents.read_parameter.parameter = request->contents.read_parameter.parameter;
  response->contents.read_parameter.param_kind = request->contents.read_parameter.param_kind;

  int nbytesread = 0;
  int res = 0;

  switch (request->contents.read_parameter.param_kind) {
  case PKIND_BYTES:
    res = param_get_bytes(request->contents.read_parameter.parameter,
      response->contents.read_parameter.bytes_val,
      &nbytesread);
    if (!res) {
      response->contents_len = 3 + nbytesread;
    } else {
      response->contents.read_parameter.param_kind = PKIND_ERR;
      response->contents_len = 3;
    }
    break;
  case PKIND_INT32:
    res = param_get_int32(request->contents.read_parameter.parameter,
      &response->contents.read_parameter.int_val);
    if (!res) {
      response->contents_len = 7;
    } else {
      response->contents.read_parameter.param_kind = PKIND_ERR;
      response->contents_len = 3;
    }
    break;
  case PKIND_BOOL:
    res = param_get_bool(request->contents.read_parameter.parameter,
      &response->contents.read_parameter.bool_val);
    if (!res) {
      response->contents_len = 4;
    } else {
      response->contents.read_parameter.param_kind = PKIND_ERR;
      response->contents_len = 3;
    }
    break;
  default:
    response->contents.read_parameter.param_kind = PKIND_ERR;
    response->contents_len = 3;
  }
}

void new_scan_request(int requested_scan_uid)
{ // this gets called when a new request for a scan comes in.
  
  // lambert: use this function to start a scan. You will need
  // to hold onto the value of requested_scan_uid so that you
  // can send a response later.
}

void send_scan_response(int requested_scan_uid, float peak1, float peak2, float peak3, float peak4)
{ // sends the response to a scan request.
  
  // lambert: when
  
  static uint8_t txbuf[64] = {};
  ResponsePacket rp = {};
  rp.kind = PK_READ_PARAMETER;
  rp.uid = requested_scan_uid;
  rp.revision = PACKET_PROTOCOL_REVISION;
  rp.contents_len = 19;
  rp.contents.read_parameter.parameter = PARAM_SINGLE_SCAN;
  rp.contents.read_parameter.param_kind = PKIND_BYTES;
  
  // convert peaks to uint32 without bit-level conversion. this limits 
  // the protocol to hosts and devices that support IEEE 754 32-bit
  // floating point numbers.
  uint32_t p[4] = {
    *((uint32_t*)&peak1),
    *((uint32_t*)&peak2),
    *((uint32_t*)&peak3),
    *((uint32_t*)&peak4)
  };
  
  for (int i = 0; i < 4; ++i) {
    // store each value in the packet, big-endian order as usual
    rp.contents.read_parameter.bytes_val[i * 4] = ((p[i] >> 24) & 0xFF);
    rp.contents.read_parameter.bytes_val[(i * 4) + 1] = ((p[i] >> 16) & 0xFF);
    rp.contents.read_parameter.bytes_val[(i * 4) + 2] = ((p[i] >> 8) & 0xFF);
    rp.contents.read_parameter.bytes_val[(i * 4) + 3] = (p[i] & 0xFF);
  }
  
  int txlen = packet_encode_response(txbuf, &rp);
  if (txlen > 0)
    usbserial_write(txbuf, txlen);
}


int debugf(char const *fmt, ...)
{
  static uint8_t buf[250];
  static uint8_t buf2[PACKET_RESPONSE_MAX_LEN];
  ResponsePacket rp;
  va_list ap;
  rp.kind = PK_PRINT;
  rp.uid = 0;
  rp.revision = PACKET_PROTOCOL_REVISION;
  va_start(ap, fmt);
  int nbytes = vsnprintf(buf, 250, fmt, ap);
  rp.contents.print.str = buf;
  va_end(ap);
  if (nbytes > 0) {
    rp.contents_len = nbytes;
    nbytes = packet_encode_response(buf2, &rp);
    if (nbytes > 0)
      usbserial_write(buf2, nbytes);
  }
  return nbytes;
}





int main()
{
  // these make the libraries work. nothing should go before this stuff
  // APB1 clock divider is 4, APB2 clock divider is 2
  // running at 216MHz, so: cycle time is 4.63ns
  //                        APB1 is 54MHz
  //                        APB2 is 108MHz
  TM_RCC_InitSystem();
  HAL_Init();
  TM_DELAY_Init();
  if (TM_USB_Init()) {
    while(1);
  }
  // end of library init

  // initialization of firmware components.
  if (mmic_periphs_init()) {
    // initialization of the mmic stuff failed.
    while (1);
  }

  if (usbserial_init()) {
    // initialization of the usb port stuff failed.
    while (1);
  }
  // end of our firmware init things

  // start Michael's DSP setup code
  float32_t time = 0; // iime for fake sine
  float32_t freq = 250000; // frequency for fake sine
  float32_t sampleShift = 1/(2*(float32_t)400000); // time shift per sample
  float32_t adcData[SAMPLES];
  int i = 0;

  // dsp.c code
  if(initialize_DSP(&dsp_obj, &dsp_ret_obj))
  {
    while(1);
  }
  // end Michael's DSP setup code


  // main loop
  static uint8_t usb_rx_buf[64] = {}; // static to keep the stack small
  static uint8_t usb_tx_buf[64] = {};
  int bufpos = 0;
  RequestPacket request;
  ResponsePacket response;
  while (1) {
    // outside the below if statement runs repeatedly, as fast as possible.
    if (tick_received) {
      tick_received = 0;
      // stuff inside this if statement will run once every millisecond.

      usbserial_periodic_task();

      if (bufpos < 64) { // read bytes into the buffer if the buffer isn't full
        int r = usbserial_read(usb_rx_buf + bufpos, 64 - bufpos);
        if (r > 0)
          bufpos += r;
      }

      int dres = packet_decode_request(usb_rx_buf, bufpos, &request);
      if (dres > 0) {
        // got a new packet, shift buffer
        for (int i = dres; i < bufpos; ++i)
          usb_rx_buf[i - dres] = usb_rx_buf[i];
        bufpos -= dres;
        // `request` now contains a new request that needs to be handled.

        if (request.kind == PK_ECHO) {
          response.kind = PK_ECHO;
          response.uid = request.uid;
          response.revision = PACKET_PROTOCOL_REVISION;
          response.contents_len = 5;
          strcpy(response.contents.echo.message, "echo");
          int txlen = packet_encode_response(usb_tx_buf, &response);
          if (txlen > 0)
            usbserial_write(usb_tx_buf, txlen);
        } else if (request.kind == PK_READ_PARAMETER) {
          if (request.contents.read_parameter.parameter == PARAM_SINGLE_SCAN) {
            // a new scan has been requested
            new_scan_request(request.uid);
          } else {
            handle_read_request(&request, &response);
            int txlen = packet_encode_response(usb_tx_buf, &response);
            if (txlen > 0)
              usbserial_write(usb_tx_buf, txlen);
          }
        }

      } else if (dres < 0) {
        asm volatile("nop"); // maybe put a while (1) here?
      }
    }



    // From here down this is MICHAEL'S looped DSP code
    i = 0;
    // Let's fake sinus signal with 5, 15 and 30Hz frequencies
    do {
      // Calculate sinus value
      sin_val = 0;
      sin_val += (float)5 * (float)sin((float)2 * (float)3.14159265359 * (float)freq * (float)time) + 1;

      time+= sampleShift;
      dsp_obj.adc_outputIQ[2*i] = sin_val;
      dsp_obj.adc_outputIQ[2*i + 1] = 0;
      i++;

    } while (i < SAMPLES/2);

    get_data();

    transferBuffer_adcToDSP(&dsp_obj, &adc1_buf, &adc2_buf);

    // Do FFT on signal, values at each bin and calculate max value and index where max value happened
    computeFFT(&dsp_obj);
    // pull out max magnitudes for FFT
    parseFFTArray(&dsp_obj);

    //Little delay
//     Delayms(50);

    // end Michael's looped DSP code

  }

  return 0;
}
