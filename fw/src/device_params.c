#include <device_params.h>
#include "stm32f7xx_hal_flash_ex.h"
#include "stm32f7xx_hal_flash.h"

#define DEVICE_PARAMS_FLASH_ADDR 0x27F000
#define DEVICE_PARAMS_FLASH ((ParmTblEntry*)DEVICE_PARAMS_FLASH_ADDR)

#if defined(STM32F722xx)
#define DEVICE_ID                    ((uint8_t*)0x1FF07A10)
#else
#warning "not sure where the device serial number is, using default. this may cause hard faults."
#define DEVICE_ID                    ((uint8_t*)0x1FFF7A10)
#endif

typedef struct HookArg {
  ParamKind param_kind;
  union {
    int32_t int_val;
    int bool_val;
    uint8_t bytes_val[16];
  };
  int bytes_len;
} HookArg;

typedef struct HookTableEntry {
  // the getter will receive a HookArg object. the object's
  // param_kind will hold the requested parameter kind. if
  // this parameter kind is wrong, set param_kind to PKIND_ERR.
  // otherwise, the getter should set a member of the union to
  // the correct value.
  void (*get)(HookArg *);
  // the setter will receive a HookArg object. the object's
  // param_kind will hold the requested parameter kind. if
  // this parameter kind is wrong, set param_kind to PKIND_ERR.
  // otherwise, the setter can set the parameter by
  // reading from the object's union member.
  void (*set)(HookArg *);
  // if get or set is set to null, this means that the param
  // is not gettable or settable. In this case, an error is
  // automatically returned.
} HookTableEntry;

// don't touch this
typedef union ParmTblEntry {
  int32_t intval;
  uint8_t boolval;
  char strval[16];
  uint8_t bytesval[16];
} ParmTblEntry;


// TODO: put hooks in a different file, this is gonna get messy
// getter hook for the serial number
void serialnumber_getter(HookArg *arg)
{
  if (arg->param_kind != PKIND_BYTES)
    arg->param_kind = PKIND_ERR;
  else {
    for (int i = 0; i < 12; ++i) {
      arg->bytes_val[i] = DEVICE_ID[i];
    }
    arg->bytes_len = 12;
  }
}

void single_shot_scan_getter(HookArg *arg)
{
  arg->param_kind = PKIND_ERR;
}


// the hook table
static HookTableEntry parmhooks[] = {
  { .get = serialnumber_getter, .set = NULL },
  { .get = single_shot_scan_getter, .set = NULL }
};



// don't touch this either
static ParmTblEntry parmtbl[256];

int param_init()
{
  for (int i = 0; i < 256; ++i) {
    for (int ao = 0; ao < 16; ++ao) {
      parmtbl[i].bytesval[ao] = DEVICE_PARAMS_FLASH[i].bytesval[ao];
    }
  }
  return 0;
}


int param_get_int32(int parameter, int32_t *value)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    *value = parmtbl[parameter].intval;
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].get == NULL) {
      result = 3; // this param is not gettable
    } else {
      HookArg ha = {
        .param_kind = PKIND_INT32
      };
      parmhooks[parameter - 0x100].get(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
      else {
        *value = ha.int_val;
        result = 0;
      }
    }
  }
  return result;
}


int param_set_int32(int parameter, int32_t value)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    parmtbl[parameter].intval = value;
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].set == NULL) {
      result = 3; // not settable
    } else {
      HookArg ha = {
        .param_kind = PKIND_INT32,
        .int_val = value
      };
      parmhooks[parameter - 0x100].set(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
    }
  }
  return result;
}


int param_get_bool(int parameter, int *value)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    *value = parmtbl[parameter].boolval;
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].get == NULL) {
      result = 3; // this param is not gettable
    } else {
      HookArg ha = {
        .param_kind = PKIND_BOOL
      };
      parmhooks[parameter - 0x100].get(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
      else {
        *value = ha.bool_val;
        result = 0;
      }
    }
  }
  return result;
}


int param_set_bool(int parameter, int value)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    parmtbl[parameter].boolval = (uint8_t)(value != 0);
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].set == NULL) {
      result = 3; // not settable
    } else {
      HookArg ha = {
        .param_kind = PKIND_BOOL,
        .bool_val = value != 0
      };
      parmhooks[parameter - 0x100].set(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
    }
  }
  return result;
}


int param_get_bytes(int parameter, uint8_t *value, int *ngotten)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    memcpy(value, parmtbl[parameter].bytesval, 16);
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].get == NULL) {
      result = 3; // this param is not gettable
    } else {
      HookArg ha = {
        .param_kind = PKIND_BYTES
      };
      parmhooks[parameter - 0x100].get(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
      else {
        *ngotten = ha.bytes_len;
        memcpy(value, ha.bytes_val, ha.bytes_len);
        result = 0;
      }
    }
  }
  return result;
}


int param_set_bytes(int parameter, uint8_t *value, int n)
{
  int result = 1;
  if (parameter < PARAM_N_PERSISTENT) {
    memcpy(parmtbl[parameter].bytesval, value, n);
    result = 0;
  } else if ((parameter < PARAM_END_HOOK) && (parameter >= 0x100)) {
    if (parmhooks[parameter - 0x100].set == NULL) {
      result = 3; // not settable
    } else {
      HookArg ha = {
        .param_kind = PKIND_BYTES,
      };
      memcpy(ha.bytes_val, value, n);
      parmhooks[parameter - 0x100].set(&ha);
      if (ha.param_kind == PKIND_ERR)
        result = 2; // incorrect type
    }
  }
  return result;
}


__weak void param_commithook_before_erase()
{
  // do not modify this function. Instead, implement
  //    void param_commithook_before_erase() {...}
  // in main.c. The function will be called right before
  // param_commit() erases flash sector 7.
  // when this is called, FLASH is locked.
}

__weak void param_commithook_after_program()
{
  // do not modify this function. Instead, implement
  //    void param_commithook_before_erase() {...}
  // in main.c. The function will be called right after
  // param_commit() has written device params to flash.
  // when this is called, FLASH is unlocked.
}


void param_commit()
{
  param_commithook_before_erase();
  
  uint32_t err;
  FLASH_EraseInitTypeDef FLASH_EraseInitStruct = {
    .TypeErase = FLASH_TYPEERASE_SECTORS,
    .Sector = (uint32_t)FLASH_SECTOR_7,
    .NbSectors = 1,
    .VoltageRange = FLASH_VOLTAGE_RANGE_3
  };
  HAL_FLASHEx_Erase(&FLASH_EraseInitStruct, &err);
  
  if (HAL_FLASH_Unlock() == HAL_OK) {
    unsigned int addr = DEVICE_PARAMS_FLASH_ADDR;
    for (int i = 0; i < 256; ++i) {
      for (int ao = 0; ao < 16; ++ao) {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, addr + ao, parmtbl[i].bytesval[ao]);
      }
      addr += 16;
    }
    param_commithook_after_program();
    HAL_FLASH_Lock();
  }
}
