#include "defines.h"

void NMI_Handler(void)
{

}

void HardFault_Handler(void)
{

}

void BusFault_Handler(void)
{

}

void UsageFault_Handler(void)
{

}

void SysTick_Handler(void) {
  /* Increase tick counter for HAL drivers */
  HAL_IncTick();
}

