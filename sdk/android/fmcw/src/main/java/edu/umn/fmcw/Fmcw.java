package edu.umn.fmcw;

import android.content.Context;

public final class Fmcw {
  private static Fmcw singleton;

  public static Fmcw getSingleton() {
    return singleton;
  }

  public static Fmcw createSingleton(Context context) {
    if (singleton == null)
      singleton = new Fmcw(context);
    return singleton;
  }

  //
  private Context applicationContext;

  private Fmcw(Context context) {
    applicationContext = context.getApplicationContext();
  }

  public Context getApplicationContext() {
    return applicationContext;
  }
}
