package edu.umn.fmcw;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Parcel;
import android.os.Parcelable;

import com.hoho.android.usbserial.driver.CdcAcmSerialDriver;
import com.hoho.android.usbserial.driver.ProbeTable;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.util.ArrayList;
import java.util.List;


public final class Util {
  private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

  /**
   * Get a String with the hex representation of a byte.
   * @param b A byte.
   * @return The byte's string representation (e.g. "7F")
   */
  public static String byteToHexString(byte b) {
    char[] result = {0, 0};
    result[0] = HEX_ARRAY[(b >> 4) & 0xF];
    result[1] = HEX_ARRAY[b & 0xF];
    return new String(result);
  }

  /**
   * Get a String with the hex representation of an array of bytes.
   *
   * When `reversed` is false, index 0 of the array is the first byte in the
   * output string. When `reversed` is false, index 0 of the array is the last
   * byte in the output string.
   *
   * {@code
   *  byte bbs[] = {(byte)0xDE, (byte)0xAD, (byte)0xBE, (byte)0xEF};
   *
   *  assert bytesToHexString(bbs, false).equals("DEADBEEF");
   *  assert bytesToHexString(bbs, true).equals("EFBEADDE");
   * }
   *
   * @param bs An array of bytes.
   * @param reversed True if the order of bytes in the output should be reversed.
   * @return The hex representation of each byte in bs, concatenated together.
   */
  public static String bytesToHexString(byte[] bs, boolean reversed) {
    StringBuilder result = new StringBuilder();
    for (byte b : bs) {
      if (reversed)
        result.insert(0, byteToHexString(b));
      else
        result.append(byteToHexString(b));
    }

    return result.toString();
  }

  /**
   * Pretty-print an array of bytes.
   *
   * Ordering of bytes in relation to `reversed` works the same as
   * {@link Util#bytesToHexString}.
   *
   * {@code
   *  byte bbs[] = {(byte)0xDE, (byte)0xAD, (byte)0xBE, (byte)0xEF};
   *
   *  assert bytesToPrettyHexString(bbs, false).equals("[DE, AD, BE, EF]");
   *  assert bytesToPrettyHexString(bbs, true).equals("[EF, BE, AD, DE]");
   * }
   *
   * @param bs An array of bytes.
   * @param reversed True if the order of bytes in the output should be reversed.
   * @return A human-friendly string representation of the array of bytes.
   */
  public static String bytesToPrettyHexString(byte[] bs, boolean reversed) {
    StringBuilder result = new StringBuilder();
    result.append('[');
    for (int i = 0; i < bs.length; ++i) {
      if (reversed)
        result.append(byteToHexString(bs[(bs.length - 1) - i]));
      else
        result.append(byteToHexString(bs[i]));

      if (i < bs.length - 1)
        result.append(", ");
      else
        result.append("]");
    }
    return result.toString();
  }

  static void assertion(boolean b, String s) {
    if (!b)
      throw new AssertionError(s);
  }

  /**
   * Return a list of FoundDevices. If you want to talk to a particular device,
   * call createDevice() on it.
   * @param context the context to associate with the FoundDevices in the return value.
   * @return a list of FoundDevices which can be opened using createDevice()
   */
  public static List<FmcwDevice.FoundDevice> findDevices(Context context) {
    List<FmcwDevice.FoundDevice> result = new ArrayList<>();
    UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    ProbeTable probeTable = new ProbeTable();
    // add vendor IDs and product IDs associated with FMCW radar devices here
    // STMicro USB VCP
    probeTable.addProduct(0x0483, 0x5740, CdcAcmSerialDriver.class);

    UsbSerialProber prober = new UsbSerialProber(probeTable);

    List<UsbSerialDriver> availableDrivers = prober.findAllDrivers(manager);
    for (UsbSerialDriver driver : availableDrivers) {
      result.add(new FmcwUsbSerialDevice.FoundUsbSerialDevice(context, manager, driver));
    }

    return result;
  }
}
