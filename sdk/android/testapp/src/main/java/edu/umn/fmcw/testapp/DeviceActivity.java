package edu.umn.fmcw.testapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import edu.umn.fmcw.Fmcw;
import edu.umn.fmcw.FmcwDevice;
import edu.umn.fmcw.FmcwUsbSerialDevice;

public class DeviceActivity extends AppCompatActivity {
  public static final String EXTRA_FOUND_DEVICE = "edu.umn.fmcw.testapp.FOUND_DEVICE";

  public FmcwDevice device = null;
  boolean openedByExternalIntent;
  ViewPager2 viewPager;
  TabLayout tabLayout;
  ViewPagerAdapter pagerAdapter;

  private final FmcwDevice.OpenCompletion openCompletion =
    (didSucceed, e) -> {
    };

  private final FmcwDevice.CloseCompletion closeCompletion =
    (didSucceed, e) -> {
      if (!didSucceed) {
        Toast.makeText(this, "Closing connection failed", Toast.LENGTH_SHORT).show();
      }
    };


  public class ViewPagerAdapter extends FragmentStateAdapter {
    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
      super(fragmentActivity);
    }

    @NonNull @Override public Fragment createFragment(int position) {
      switch (position) {
        case 0: return DeviceScanFragment.createInstance();
        default: return DeviceConfigFragment.createInstance();
      }
    }

    @Override public int getItemCount() {
      return 2;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_device);

    Fmcw.createSingleton(this);

    UsbDevice deviceFromIntent = getIntent().getParcelableExtra(UsbManager.EXTRA_DEVICE);
    if (deviceFromIntent != null) {
      device = new FmcwUsbSerialDevice(this, deviceFromIntent);
      openedByExternalIntent = true;
    } else {
      FmcwDevice.FoundDevice foundDevice = getIntent().getParcelableExtra(EXTRA_FOUND_DEVICE);
      device = foundDevice.createDevice();
      openedByExternalIntent = false;
    }

    viewPager = findViewById(R.id.device_pager);
    tabLayout = findViewById(R.id.device_tabs);


    pagerAdapter = new ViewPagerAdapter(this);
    viewPager.setAdapter(pagerAdapter);
    new TabLayoutMediator(tabLayout, viewPager,
      (tab, position) -> {
        switch (position) {
          case 0: tab.setText(getString(R.string.scan)); break;
          case 1: tab.setText(getString(R.string.configure)); break;
        }
      }).attach();

    Toolbar toolbar = findViewById(R.id.device_toolbar);
    toolbar.setTitle(device.getProductName());
    setSupportActionBar(toolbar);
  }

  @Override
  protected void onResume() {
    super.onResume();
    device.open(openCompletion);
  }

  @Override
  protected void onPause() {
    super.onPause();
    device.close(closeCompletion);
  }
}
