package edu.umn.fmcw.testapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.snackbar.Snackbar;

import edu.umn.fmcw.FmcwDevice;

public class DeviceScanFragment extends Fragment {
  FmcwDevice device;
  public DeviceScanFragment() {
    Log.d("edu.umn.fmcw.testapp", "DeviceScanFragment made");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    DeviceActivity activity = (DeviceActivity) getActivity();
    device = activity.device;
    return inflater.inflate(R.layout.fragment_device_scan, container, false);
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    view.findViewById(R.id.start_scan_button).setOnClickListener(v -> {
      device.scan((didSucceed, freqs, message) -> {
        if (!didSucceed) {
          Snackbar.make(v, getString(R.string.echo_test_success) + "(" + (message != null ? message : "null") + ")", Snackbar.LENGTH_SHORT).show();
        } else {
          ((TextView)view.findViewById(R.id.result_1)).setText(String.valueOf(freqs[0]));
          ((TextView)view.findViewById(R.id.result_2)).setText(String.valueOf(freqs[1]));
          ((TextView)view.findViewById(R.id.result_3)).setText(String.valueOf(freqs[2]));
          ((TextView)view.findViewById(R.id.result_4)).setText(String.valueOf(freqs[3]));
          Snackbar.make(view, getString(R.string.scan_complete), Snackbar.LENGTH_SHORT).show();
        }
      });
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    DeviceActivity activity = (DeviceActivity) getActivity();
    device = activity.device;
  }

  public static DeviceScanFragment createInstance() {
    DeviceScanFragment result = new DeviceScanFragment();
    return result;
  }
}
